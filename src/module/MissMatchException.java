package module;

import javax.swing.JOptionPane;

public class MissMatchException extends Exception {

	/**
	* 
	*/
	private static final long serialVersionUID = -2686408653436225694L;

	public MissMatchException() {

	}

	public MissMatchException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
		JOptionPane.showInternalMessageDialog(null, message, "Error", JOptionPane.ERROR_MESSAGE);
	}

	public MissMatchException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public MissMatchException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public MissMatchException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
