package module;

import java.time.LocalDate;
import java.util.regex.Pattern;

/**
 * @author DuwigN
 *
 */
/**
 * @author fanny
 *
 */
public class Client extends Personne {

	private String numSecu;
	private LocalDate dateNaissance;
	private String mutuelle;

	/**
	 * Constructeur de ma classe
	 * 
	 * @param nom
	 * @param prenom
	 * @param adresse
	 * @param codePostal
	 * @param ville
	 * @param telephone
	 * @param email
	 * @param numSecu
	 * @param dateNaissance
	 * @param mutuelle
	 * @throws MissMatchException
	 */
	public Client(String nom, String prenom, String adresse, String codePostal, String ville, String telephone,
			String email, String numSecu, LocalDate dateNaissance, String mutuelle) throws MissMatchException {
		super(nom, prenom, adresse, codePostal, ville, telephone, email);
		this.setNumSecu(numSecu);
		this.setDateNaissance(dateNaissance);
		this.setMutuelle(mutuelle);
	}

	/**
	 * methode pour un affichage d'une comboBox panel AchatDirect
	 * 
	 * @return : Nom Prenom
	 */
	@Override
	public String toString() {
		return this.getNom() + " " + getPrenom();
	}

	// getter et setter de la classe Client.
	// comparatifs à l'aide de RegEx dans les exections

	public String getNumSecu() {
		return numSecu;
	}

	public void setNumSecu(String numSecu) throws MissMatchException {
		if (numSecu == null || numSecu.trim().isEmpty()) {
			throw new MissMatchException("Saisie du numéro de sécurité sociale incorrecte");
		}
		if (!Pattern.matches("[1-2][0-9]{14}", numSecu)) {
			throw new MissMatchException("Veuillez entrer un numéro de sécurité sociale valide ");
		} else {
			this.numSecu = numSecu;
		}
	}

	public LocalDate getDateNaissance() {
		return dateNaissance;
	}

	public void setDateNaissance(LocalDate dateNaissance) throws MissMatchException {
		if (dateNaissance == null || dateNaissance.toString().trim().isEmpty()) {
			throw new MissMatchException("Saisie de la date de naissance incorrecte");
		}
		if (!Pattern.matches("^\\d{4}-\\d{2}-\\d{2}", dateNaissance.toString())) {
			throw new MissMatchException("Veuillez entrer un format de date valide : année/mois/jour ");
		} else {
			this.dateNaissance = dateNaissance;
		}
	}

	public String getMutuelle() {
		return mutuelle;
	}

	public void setMutuelle(String mutuelle) throws MissMatchException {
		if (mutuelle == null || mutuelle.trim().isEmpty()) {
			throw new MissMatchException("Saisie de la mutuelle incorrecte");
		}
//		if(!Pattern.matches("", mutuelle)) {
//			throw new MissMatchException("Veuillez entrer une mutuelle valide");
//		}
		else {
			this.mutuelle = mutuelle;
		}
	}
}
