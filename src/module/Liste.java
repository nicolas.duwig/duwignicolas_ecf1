package module;

/** 
 *  Déclarations des imports
 */
import java.util.ArrayList;

/**
 * @author DuwigN
 *
 */
public class Liste {

	// création de toutes les listes pour enregistrer les données
	private ArrayList<Client> listeClient = new ArrayList<Client>();
	private ArrayList<Medecin> listeMedecin = new ArrayList<Medecin>();
	private ArrayList<Specialiste> listeSpecialiste = new ArrayList<Specialiste>();
	private ArrayList<Medicament> listeMedicament = new ArrayList<Medicament>();
	private ArrayList<Achat> listeAchat = new ArrayList<Achat>();
	private ArrayList<String> listeJours = new ArrayList<>();
	private ArrayList<String> listeMois = new ArrayList<>();
	private ArrayList<String> listeAnnée = new ArrayList<>();

	/**
	 * Constructeur de ma classe
	 */
	public Liste() {

	}

	// getter et setter de la classe Liste
	public ArrayList<Client> getListeClient() {
		return listeClient;
	}

	public void setListeClient(ArrayList<Client> listeClient) {
		this.listeClient = listeClient;
	}

	/**
	 * @param pClient ajoute le client à la liste des clients
	 */
	public void ajouterClient(Client pClient) {
		this.getListeClient().add(pClient);
	}

	public ArrayList<Medecin> getListeMedecin() {
		return listeMedecin;
	}

	public void setListeMedecin(ArrayList<Medecin> listeMedecin) {
		this.listeMedecin = listeMedecin;
	}

	/**
	 * @param pMedecin ajoute le medecin à la liste des medecin
	 */
	public void ajouterMedecin(Medecin pMedecin) {
		this.getListeMedecin().add(pMedecin);
	}

	public ArrayList<Specialiste> getListeSpecialiste() {
		return listeSpecialiste;
	}

	public void setListeSpecialiste(ArrayList<Specialiste> listeSpecialiste) {
		this.listeSpecialiste = listeSpecialiste;
	}

	/**
	 * @param pSpecialiste ajoute le specialiste à la liste des specialistes
	 */
	public void ajouterSpecialiste(Specialiste pSpecialiste) {
		this.getListeSpecialiste().add(pSpecialiste);
	}

	public ArrayList<Medicament> getListeMedicament() {
		return listeMedicament;
	}

	public void setListeMedicament(ArrayList<Medicament> listeMedicament) {
		this.listeMedicament = listeMedicament;
	}

	/**
	 * @param pMedicament ajoute le medicament à la liste des medicament
	 */
	public void ajouterMedicament(Medicament pMedicament) {
		this.getListeMedicament().add(pMedicament);
	}

	public ArrayList<Achat> getListeAchat() {
		return listeAchat;
	}

	public void setListeAchat(ArrayList<Achat> listeAchat) {
		this.listeAchat = listeAchat;
	}

	/**
	 * @param pAchat ajote le client à la liste des clients
	 */
	public void ajoutereAchat(Achat pAchat) {
		this.getListeAchat().add(pAchat);
	}

}
