package module;

import java.time.LocalDate;
import java.util.regex.Pattern;

/**
 * @author DuwigN
 *
 */
public class Medicament {
	private String nomMedoc;
	private String categorie;
	private double prix;
	private LocalDate date_MeS;
	private int quantite;

	/**
	 * Constructeur de a classe
	 * 
	 * @param nomMedoc
	 * @param categorie
	 * @param prix
	 * @param date_MeS
	 * @param quantite
	 * @throws MissMatchException
	 */
	public Medicament(String nomMedoc, String categorie, double prix, LocalDate date_MeS, int quantite)
			throws MissMatchException {
		this.setNom(nomMedoc);
		this.setCategorie(categorie);
		this.setPrix(prix);
		this.setDate_MeS(date_MeS);
		this.setQuantite(quantite);
	}

	public String toString() {
		return this.getnomMedoc();
	}

	// getter et setter de la classe Medicament
	// comparatifs à l'aide de RegEx dans les exceptions
	public String getnomMedoc() {
		return nomMedoc;
	}

	public void setNom(String nomMedoc) throws MissMatchException {
		if (nomMedoc == null || nomMedoc.trim().isEmpty()) {
			throw new MissMatchException("Saisie du nom du médicament incorrecte");
		}
		if (!Pattern.matches("([a-zA-Z]+[-éèçàîêôë]*)+", nomMedoc)) {
			throw new MissMatchException("Veuillez entrer un nom du médicament valide ");
		} else {
			this.nomMedoc = nomMedoc;
		}
	}

	public String getCategorie() {
		return categorie;
	}

	public void setCategorie(String categorie) throws MissMatchException {
		if (categorie == null || categorie.trim().isEmpty()) {
			throw new MissMatchException("Saisie de la categorie de médicament incorrecte");
		}
		if (!Pattern.matches("([a-zA-Z]+[-éèçàîêôë]*)+", categorie)) {
			throw new MissMatchException("Veuillez entrer une categorie de médicament valide ");
		} else {
			this.categorie = categorie;
		}
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) throws MissMatchException {
		if (String.valueOf(prix) == null || String.valueOf(prix).trim().isEmpty()) {
			throw new MissMatchException("Saisie du prix incorrecte");
		}
		if (!Pattern.matches("^[0-9]+\\.?[0-9]*$", String.valueOf(prix))) {
			throw new MissMatchException("Veuillez entrer un prix valide ");
		} else {
			this.prix = prix;
		}
	}

	public LocalDate getDate_MeS() {
		return date_MeS;
	}

	public void setDate_MeS(LocalDate date_MeS) throws MissMatchException {
		if (date_MeS == null || date_MeS.toString().trim().isEmpty()) {
			throw new MissMatchException("Saisie de la date de mise en service incorrecte");
		}
		if (!Pattern.matches("^\\d{4}-\\d{2}-\\d{2}", date_MeS.toString())) {
			throw new MissMatchException("Veuillez entrer un format de date valide : année/mois/jour ");
		} else {
			this.date_MeS = date_MeS;
		}
	}

	public int getQuantite() {
		return quantite;
	}

	public void setQuantite(int quantite) throws MissMatchException {
		if (String.valueOf(quantite) == null || String.valueOf(quantite).trim().isEmpty()) {
			throw new MissMatchException("Saisie de la quantite incorrecte");
		}
		if (!Pattern.matches("^\\d+$", String.valueOf(quantite))) {
			throw new MissMatchException("Veuillez entrer une quantite valide ");
		} else {
			this.quantite = quantite;
		}
	}
}
